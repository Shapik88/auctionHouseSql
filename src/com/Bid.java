package com;

/**
 * Created by Евгений on 11.08.2017.
 */
public class Bid {
    private int id;
    private String name;
    public int lotId;
    public int buyerId;
    public int firstBet;
    public int secondBet;
    public int thirdBet;
    public int lastBet;

    public Bid(int id, String name, int lotId, int buyerId, int firstBet, int secondBet, int thirdBet, int lastBet){
        this.id = id;
        this.name = name;
        this.lotId = lotId;
        this.buyerId = buyerId;
        this.firstBet = firstBet;
        this.secondBet = secondBet;
        this.thirdBet = thirdBet;
        this.lastBet = lastBet;
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }
    public int getLotId(){
        return lotId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public int getFirstBet() {
        return firstBet;
    }

    public int getSecondBet() {
        return secondBet;
    }

    public int getThirdBet() {
        return thirdBet;
    }

    public int getLastBet() {
        return lastBet;
    }
    @Override
    public String toString(){
        return getClass().getSimpleName() + "id: " +id + " Name: " +name+ " lotId: " + lotId+ " buyerId: "+ buyerId+
                " firstBet: "+firstBet+ " secondBet:"+ secondBet+
                " thirdBet: "+thirdBet+ " lastBet:"+ lastBet;

    }




}

