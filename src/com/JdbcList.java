package com;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class JdbcList {
    String queryBuyer = "select * from buyer";
    String querySeller = "select * from seller";
    String queryLot = "select * from lot";
    String queryBid = "select * from bid";
    JdbcConnection jdbcConnection = new JdbcConnection();

    public List<Buyer> buyer() throws SQLException {
        Statement statement = jdbcConnection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(queryBuyer);

        List<Buyer> buyerList = new ArrayList<>();
        while(resultSet.next()){
            Buyer buyer = new Buyer(resultSet.getInt(1), resultSet.getString(2));
            buyerList.add(buyer);
        }
        return  buyerList;
    }
    public List<Seller> seller() throws SQLException{
        Statement statement = jdbcConnection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(querySeller);
        List<Seller> sellerList = new ArrayList<>();
        while(resultSet.next()){
            Seller seller = new Seller(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
            sellerList.add(seller);
        }
        return  sellerList;
    }
    public List<Lot> lot() throws SQLException{
        Statement statement = jdbcConnection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(queryLot);
        List<Lot> lotList = new ArrayList<>();
        while(resultSet.next()){
            Lot lot = new Lot(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3));
            lotList.add(lot);
        }
        return  lotList;

    }
    public List<Bid> bid() throws SQLException{
        Statement statement = jdbcConnection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(queryBid);
        List<Bid> bidList = new ArrayList<>();
        while(resultSet.next()){
            Bid bid = new Bid(resultSet.getInt(1), resultSet.getString(2),
                    resultSet.getInt(3), resultSet.getInt(4), resultSet.getInt(5),
                    resultSet.getInt(6), resultSet.getInt(7), resultSet.getInt(8));
            bidList.add(bid);
        }
        return  bidList;
    }


}
